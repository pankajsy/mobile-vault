package com.example.mobile.mobsec1nov.androidsqlite;

import java.io.Serializable;

public class Entryfield implements Serializable {
	//private variables
	int _id;
	String folder;
	String title;
	String username;
	String password;
	String url;
	String notes;
	String email;
	public Entryfield(){

	}
	// constructor
	public Entryfield(int id, String folder, String email, String username, String password, String url, String notes, String title){
		this._id = id;
		this.folder = folder;
		this.email = email;
		this.username = username;
		this.password = password;
		this.url = url;
		this.notes = notes;
		this.title = title;
	}

//	// constructor
	public Entryfield(String folder, String email, String username, String password, String url, String title, String notes){
        this.folder = folder;
        this.email = email;
        this.username = username;
        this.password = password;
        this.url = url;
        this.notes = notes;
        this.title = title;
	}

public int getID(){ return this._id;}
    public void setID(int id){ this._id = id;}

    public String getFolderName(){return this.folder;	}
    public void setFolderName(String name){this.folder = name;}

    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }

    public String getPassword(){
        return this.password;
    }
    public void setPassword(String password){
        this.password = password;
    }

    public String getUsername(){
        return this.username;
    }
    public void setUsername(String username){
        this.username = username;
    }

    public String getUrl(){
        return this.url;
    }
    public void setUrl(String url){
        this.url = url;
    }

    public String getNotes(){
        return this.notes;
    }
    public void setNotes(String notes){
        this.notes = notes;
    }

    public String getTitle(){
        return this.title;
    }
    public void setTitle(String title){
        this.title = title;
    }


}
