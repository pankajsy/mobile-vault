package com.example.mobile.mobsec1nov.Application;

import android.app.Application;

public class MyApplication extends Application {
    private static String SALT;
    private static String KEY;

    @Override
    public void onCreate() {
        registerActivityLifecycleCallbacks(new MyLifecycleHandler());
        super.onCreate();
    }
    public static String getKEY() {
        return KEY;
    }

    public static void setKEY(String key) {
        KEY = key;
    }

    public static String getSALT() {
        return SALT;
    }

    public static void setSALT(String salt) {
        SALT = salt;
    }
}