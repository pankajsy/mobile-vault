package com.example.mobile.mobsec1nov.crypto;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by pkjsoccer on 11/30/2015.
 */
public class b {
    public static void decrypt() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "MobileVault" + File.separator+"New York University folder"+File.separator+"Nyu.txt");
        folder = new File(folder.getAbsolutePath());
        folder.getParent();
        FileInputStream fis = new FileInputStream(folder.getParent());

        FileOutputStream fos = new FileOutputStream("decrypted");
        SecretKeySpec sks = new SecretKeySpec("MyDifficultPassw".getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, sks);
        CipherInputStream cis = new CipherInputStream(fis, cipher);
        int b;
        byte[] d = new byte[8];
        while((b = cis.read(d)) != -1) {
            fos.write(d, 0, b);
        }
        fos.flush();
        fos.close();
        cis.close();
    }
}
