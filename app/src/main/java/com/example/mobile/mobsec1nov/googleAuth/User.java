package com.example.mobile.mobsec1nov.googleAuth;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    private static final long serialVersionUID = -7039852050030691867L;
    public int id;
    public String name;
    public String token;
    public String platform;
    private String userImg;
    private String emailid;
    private String profile_url;

    public  User(){}

    public User(int id, String name, String token, String email, String profile_url, String profile_url_dp, String platform) {
        this.id = id;
        this.name = name;
        this.token = token;
        this.emailid = email;
        this.platform = platform;
        this.profile_url = profile_url;
        this.userImg = profile_url_dp;

    }


    public String getprofile_url() {
        return this.profile_url;
    }
    public void setprofile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    public String getPlatform() {
        return this.platform;
    }
    public void setplatform(String platform) {
        this.platform = platform;
    }

    public int getID() {
        return this.id;
    }
    public void setID(int id) {
        this.id = id;
    }
    public String getEmailId() {
        return this.emailid;
    }
    public void setEmailId(String emailid) {
        this.emailid = emailid;
    }


    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }


    public String getToken() {
        return this.token;
    }
    public void setToken(String token) {
        this.token = token;
    }


    public String getUserImg() {
        return userImg;
    }
    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }


}