package com.example.mobile.mobsec1nov.cryptoalgorithm;

import android.content.Context;
import android.util.Base64;

import com.example.mobile.mobsec1nov.Application.MyApplication;
import com.example.mobile.mobsec1nov.ListviewEntries.GetOfflineData;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by pkjsoccer on 11/30/2015.
 */

public class AESClass_Salt2 {
//    private static final String salt = MyApplication.getSALT();
    private static final String saltsh = "34567rtyucvbn";
    private static final String keysh = "567890987iuytr";
//    private static final String key = MyApplication.getKEY();
    private static int keySize = 256;
    private byte[] ivBytes;
    public static String decryptString(Context context, String textToDecrypt) {
        String key = GetOfflineData.retriveOfflineResponse("MASTERKEY", context);
        String salt = GetOfflineData.retriveOfflineResponse("SALT", context);
        SecretKeySpec skeySpec = new SecretKeySpec(hmacSha1(salt, key), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] encryptedData = cipher.doFinal(Base64.decode(textToDecrypt, Base64.NO_CLOSE));
            if (encryptedData == null) return null;
            return new String(encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    // encryption method
    public static String encryptString(Context context, String clearText) {
        String key = GetOfflineData.retriveOfflineResponse("MASTERKEY", context);
        String salt = GetOfflineData.retriveOfflineResponse("SALT", context);
        SecretKeySpec skeySpec = new SecretKeySpec(hmacSha1(salt, key), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encryptedData = cipher.doFinal(clearText.getBytes());
            if (encryptedData == null) return null;
            return Base64.encodeToString(encryptedData, Base64.NO_CLOSE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

   // Key generation
    public static byte[] hmacSha1(String salt, String key) {
        SecretKeyFactory factory = null;
        Key keyByte = null;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec keyspec = new PBEKeySpec(key.toCharArray(), salt.getBytes(), 1024, keySize);
            keyByte = factory.generateSecret(keyspec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return keyByte.getEncoded();
    }

    public static String decryptSharedString(String textToDecrypt) {
        SecretKeySpec skeySpec = new SecretKeySpec(hmacSha1(saltsh, keysh), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] encryptedData = cipher.doFinal(Base64.decode(textToDecrypt, Base64.NO_CLOSE));
            if (encryptedData == null) return null;
            return new String(encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // encryption method
    public static String encryptSharedString(String clearText) {
        SecretKeySpec skeySpec = new SecretKeySpec(hmacSha1(saltsh, keysh), "AES");
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encryptedData = cipher.doFinal(clearText.getBytes());
            if (encryptedData == null) return null;
            return Base64.encodeToString(encryptedData, Base64.NO_CLOSE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
