package com.example.mobile.mobsec1nov.ListviewEntries;

import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mobile.mobsec1nov.R;
import com.example.mobile.mobsec1nov.androidsqlite.DatabaseHandler;
import com.example.mobile.mobsec1nov.androidsqlite.Entryfield;
import com.example.mobile.mobsec1nov.crypto.CryptoException;
import com.example.mobile.mobsec1nov.crypto.CryptoUtils;
import com.example.mobile.mobsec1nov.cryptoalgorithm.AESClass_Salt2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class EnterDetails extends AppCompatActivity {
    EditText password, foldername, title, username, email, url, notes;
    Button addfield, addfiles;
    String value;
    Entryfield entry;
    File inputFile, encryptedFile, decryptedFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_p1);

        foldername = (EditText)findViewById(R.id.folder_name);
        password = (EditText)findViewById(R.id.password);
        title = (EditText)findViewById(R.id.title);
        username = (EditText)findViewById(R.id.username);
        email = (EditText)findViewById(R.id.email);
        url = (EditText)findViewById(R.id.url);
        notes = (EditText)findViewById(R.id.notes);

        CheckBox c=(CheckBox)findViewById(R.id.showpass);
        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

            }
        });
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        entry = new Entryfield();
        if (getIntent().getStringExtra("Entryfield")!=null){
            entry = (Entryfield)bundle.getSerializable("Value");;
            value = getIntent().getStringExtra("Entryfield");
            if (value.equals("Update"))
              setdetails(entry);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.done);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Done modifying changes!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                set_Values_DB(entry);
            }
        });
        addfield = (Button)findViewById(R.id.more_field);
        addfield.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                inputFile = new File(getAbspath()+File.separator, entry.getTitle()+"_document.txt");
//                encryptedFile = new File(getAbspath()+File.separator,entry.getTitle()+"_document.encrypted");
//                decryptedFile = new File(getAbspath()+File.separator,entry.getTitle()+"_document.decrypted");
                inputFile = new File("doc.txt");
                encryptedFile = new File("doc.encrypted");
                decryptedFile = new File("doc.decrypted");
                try {
                    CryptoUtils.decrypt("Mary has one cat1", encryptedFile, decryptedFile);
                } catch (CryptoException ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
            }
        });
        addfiles = (Button)findViewById(R.id.add_file);

        addfiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputFile = new File("doc.txt");
                encryptedFile = new File("doc.encrypted");
                decryptedFile = new File("doc.decrypted");
                try {
                    CryptoUtils.encrypt("Mary has one cat1", inputFile, encryptedFile);
//                    CryptoUtils.decrypt("", encryptedFile, decryptedFile);
                } catch (CryptoException ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
            }
        });
    }

    private void setdetails(Entryfield entry){
        foldername.setText(entry.getFolderName());
        password.setText(entry.getPassword());
        title.setText(entry.getTitle());
        username.setText(entry.getUsername());
        email.setText(entry.getEmail());
        url.setText(entry.getUrl());
        notes.setText(entry.getNotes());
//        Toast.makeText(EnterDetails.this, "Modify your changes!!", Toast.LENGTH_LONG).show();

    }



    private void set_Values_DB(Entryfield entry) {
        DatabaseHandler db = new DatabaseHandler(this);
        if (value.equals("Add")) {
            db.addEntry(
                    new Entryfield(
                            foldername.getText().toString(),
                            email.getText().toString(),
                            username.getText().toString(),
                            password.getText().toString(),
                            url.getText().toString(),
                            title.getText().toString(),
                            notes.getText().toString()
                            ));
            createfolder(foldername.getText().toString());
            Toast.makeText(EnterDetails.this, foldername.getText().toString() + ":- folder created", Toast.LENGTH_SHORT).show();

        }else {
            Entryfield update_entry = new Entryfield();
            update_entry.setID(entry.getID());
            update_entry.setFolderName(foldername.getText().toString());
            update_entry.setEmail(email.getText().toString());
            update_entry.setPassword(password.getText().toString());
            update_entry.setUsername(username.getText().toString());
            update_entry.setUrl(url.getText().toString());
            update_entry.setTitle(title.getText().toString());
            update_entry.setNotes(notes.getText().toString());

            db.updateEntry(update_entry);
            Toast.makeText(EnterDetails.this, "Updated",Toast.LENGTH_SHORT).show();

        }
        EnterDetails.this.finish();
    }
    private void createfolder(String foldname) {
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "MobileVault");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        try{
            if (success) {
                File folderinside = new File(folder, foldname);
                folderinside.mkdir();
                File original_file = new File(folderinside, foldname+".txt");
                FileWriter writer = new FileWriter(original_file);
                writer.append("Folder Name:" + foldername.getText().toString() +
                        "\nEmail:" + email.getText().toString() +
                        "\nUsername" + username.getText().toString() +
                        "\nPassword:" + password.getText().toString() +
                        "\nUrl:" + url.getText().toString() +
                        "\nTitle:" + title.getText().toString() +
                        "\nNotes:" + notes.getText().toString());
                writer.flush();
                writer.close();


                File encrypted_file = new File(folderinside, foldname+".encrypted.txt");
                FileWriter writere = new FileWriter(encrypted_file);
                writere.append(AESClass_Salt2.encryptString(EnterDetails.this, "Folder Name:" + foldername.getText().toString() +
                        "\nEmail:" + email.getText().toString() +
                        "\nUsername" + username.getText().toString() +
                        "\nPassword:" + password.getText().toString() +
                        "\nUrl:" + url.getText().toString() +
                        "\nTitle:" + title.getText().toString() +
                        "\nNotes:" + notes.getText().toString()));
                writere.flush();
                writere.close();


                File decrypted_file = new File(folderinside, foldname+".decrypted.txt");
                FileWriter writerd = new FileWriter(decrypted_file);
                writerd.append(AESClass_Salt2.decryptString(EnterDetails.this, AESClass_Salt2.encryptString(EnterDetails.this, "Folder Name:" + foldername.getText().toString() +
                        "\nEmail:" + email.getText().toString() +
                        "\nUsername" + username.getText().toString() +
                        "\nPassword:" + password.getText().toString() +
                        "\nUrl:" + url.getText().toString() +
                        "\nTitle:" + title.getText().toString() +
                        "\nNotes:" + notes.getText().toString())));
                writerd.flush();
                writerd.close();

//            CryptoUtils.encrypt("LASTTRY", original_file, encrypted_file);
//            CryptoUtils.decrypt("LASTTRY", encrypted_file, decrypted_file);
            } else {
                Toast.makeText(EnterDetails.this, "Folder creation failed", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    private String getAbspath(){
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "MobileVault");
        folder = new File(folder.getAbsolutePath());
        return folder.getParent();

    }
}
