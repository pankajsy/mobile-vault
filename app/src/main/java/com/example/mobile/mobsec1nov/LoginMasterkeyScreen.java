package com.example.mobile.mobsec1nov;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobile.mobsec1nov.Application.MyApplication;
import com.example.mobile.mobsec1nov.ListviewEntries.GetOfflineData;
import com.example.mobile.mobsec1nov.ListviewEntries.ListViewActivity;
import com.example.mobile.mobsec1nov.crypto.a;
import com.example.mobile.mobsec1nov.crypto.b;
import com.example.mobile.mobsec1nov.googleAuth.GoogleLogin;

public class LoginMasterkeyScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginscreen);
        final EditText masterpasswprd = (EditText)findViewById(R.id.masterpassword);
        final TextView googletext = (TextView)findViewById(R.id.googletext);
//        try {
//            a.encrypt();
//            b.decrypt();
//        }catch (Exception  e){
//            e.printStackTrace();
//        }

        ImageView gp = (ImageView) findViewById(R.id.gp);
        gp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginMasterkeyScreen.this, GoogleLogin.class);
                startActivity(intent);
                LoginMasterkeyScreen.this.finish();
            }
        });


        Button masterbtn = (Button)findViewById(R.id.login);

        if (GetOfflineData.retriveOfflineResponse("SALT",this)!=null) {
            if (GetOfflineData.retriveOfflineResponse("MASTERKEY", this) != null) {

                masterbtn.setText("LOGIN");
                masterbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String password = masterpasswprd.getText().toString();
                        if (password.equals(GetOfflineData.retriveOfflineResponse("MASTERKEY", LoginMasterkeyScreen.this))) {
                            Toast.makeText(LoginMasterkeyScreen.this, "Success!!!!!", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(LoginMasterkeyScreen.this, ListViewActivity.class);
                            startActivity(intent);
                            LoginMasterkeyScreen.this.finish();
                        } else {
                            Toast.makeText(LoginMasterkeyScreen.this, "Wrong Key XXXX", Toast.LENGTH_LONG).show();
                        }

                    }
                });
            } else {
                masterbtn.setText("SET MASTERKEY");
                masterbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String password = masterpasswprd.getText().toString();
                        GetOfflineData.saveOfflineResponse("MASTERKEY", password, LoginMasterkeyScreen.this);
                        if (password.equals(GetOfflineData.retriveOfflineResponse("MASTERKEY", LoginMasterkeyScreen.this))) {
                            Toast.makeText(LoginMasterkeyScreen.this, "Remember your key: '" + password+"'", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(LoginMasterkeyScreen.this, ListViewActivity.class);
                            startActivity(intent);
                            LoginMasterkeyScreen.this.finish();
                        } else {
                            Toast.makeText(LoginMasterkeyScreen.this, "Wrong Key XXXX", Toast.LENGTH_LONG).show();
                        }

                    }
                });
                }
            gp.setVisibility(View.GONE);
            googletext.setVisibility(View.GONE);
        }else{
            masterbtn.setVisibility(View.GONE);
            masterpasswprd.setVisibility(View.GONE);
        }

    }
}
