package com.example.mobile.mobsec1nov.ListviewEntries;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.mobile.mobsec1nov.cryptoalgorithm.AESClass_Salt2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class GetOfflineData {
    private static SharedPreferences prefs;
    private static SharedPreferences.Editor prefsEditor;

    public static String retriveOfflineResponse(String preferenceName, Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String jsonString = prefs.getString(preferenceName, null);
        return AESClass_Salt2.decryptSharedString(jsonString);
    }

    public static void saveOfflineResponse(String preferenceName, String jsonString, Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = prefs.edit();
        prefsEditor.putString(preferenceName, AESClass_Salt2.encryptSharedString(jsonString));
        prefsEditor.commit();
    }

}