package com.example.mobile.mobsec1nov.googleAuth;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mobile.mobsec1nov.ListviewEntries.GetOfflineData;
import com.example.mobile.mobsec1nov.ListviewEntries.ListViewActivity;
import com.example.mobile.mobsec1nov.LoginMasterkeyScreen;
import com.example.mobile.mobsec1nov.R;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import java.io.IOException;
import java.io.InputStream;
//import android.util.Log;

public class GoogleLogin extends Activity implements ConnectionCallbacks, OnConnectionFailedListener {
    private static final int DIALOG_GET_GOOGLE_PLAY_SERVICES = 1;
    private static final int REQUEST_CODE_SIGN_IN = 1;
    private static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 2;
    private static final int RC_SIGN_IN = 0;
    private static final String TAG = "GoogleLogin";
    private static GoogleApiClient mGoogleApiClient;
    public static final String SCOPES = "https://www.googleapis.com/auth/plus.login";
    private boolean mIntentInProgress;

    private boolean mSignInClicked; //For giving the user an ability to choose an account everytime he clicks on google button
    private boolean alreadyconnected;

    private boolean dynamicSigninclick;
    private Activity activity;
    private ConnectionResult mConnectionResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.c_googleplus_sigin);
        activity = GoogleLogin.this;
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API, Plus.PlusOptions.builder()
                .addActivityTypes(MomentUtil.ACTIONS).build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }
        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;
            if (mSignInClicked) {
//            	signInWithGplus();
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            } else if (!dynamicSigninclick) {
                signInWithGplus();
            } else {
                GoogleLogin.this.finish();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        if (alreadyconnected) {
            mSignInClicked = false;
          Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
            getProfileInformation(activity);
//            updateUI(true);
        } else {
//			alreadyconnected=true;
            signOutFromGplus();
        }

    }

    private void getProfileInformation(final Activity activity) {

        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                final String google_ID = currentPerson.getId();
                final String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                final String personGooglePlusProfile = currentPerson.getUrl();
                final String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                AsyncTask<Void, Void, String> Token = new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        String accessToken = null;
                        try {
                            accessToken = GoogleAuthUtil.getToken(
                                    GoogleLogin.this,
                                    email,
                                    "oauth2:" + SCOPES);
                        } catch (IOException transientEx) {
//                            Amplitude.logEvent("Google network or server error" + transientEx.toString());
                        } catch (UserRecoverableAuthException e) {
                            Intent intent = e.getIntent();
                            if (intent != null) {
                                startActivityForResult(intent, REQUEST_CODE_SIGN_IN);
                            }
                        } catch (GoogleAuthException authEx) {
                        }
                        return accessToken;
                    }

                    @Override
                    protected void onPostExecute(String accessToken) {
                        GetOfflineData.saveOfflineResponse("SALT", google_ID+accessToken+email, activity);

//                        UserDatabaseHandler sign_up = new UserDatabaseHandler(activity);
//                        sign_up.Add_User(new User(uid, usernamep, firstname, lastname, token));
                        Toast.makeText(getApplicationContext(),"Welcome "+ GetOfflineData.retriveOfflineResponse("User", activity)+"!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(GoogleLogin.this, LoginMasterkeyScreen.class);
                        startActivity(intent);

                        GoogleLogin.this.finish();
                    }

                };
                Token.execute();
//                new LoadProfileImage(imgProfilePic).execute(personPhotoUrl);

            } else {
                Toast.makeText(getApplicationContext(),"Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_LONG).show();
            GoogleLogin.this.finish();
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            dynamicSigninclick = true;
            mSignInClicked = true;
            alreadyconnected = true;
            resolveSignInError();
        }
    }

    public static void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    private void revokeGplusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.e(TAG, "User access revoked!");
                            mGoogleApiClient.connect();
                        }

                    });
        }
    }


    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}