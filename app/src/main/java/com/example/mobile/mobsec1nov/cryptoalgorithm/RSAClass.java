package com.example.mobile.mobsec1nov.cryptoalgorithm;

import android.util.Base64;
import android.util.Log;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by pkjsoccer on 11/26/2015.
 */
public class RSAClass {
    static final String TAG = "AsymmetricAlgorithmRSA";

    public static String getEncodedString(String input, String enc_dec) {
        String encodedstring = null;
        // Generate key pair for 1024-bit RSA encryption and decryption
        Key publicKey = null;
        Key privateKey = null;
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            KeyPair kp = kpg.genKeyPair();
            publicKey = kp.getPublic();
            privateKey = kp.getPrivate();
        } catch (Exception e) {
            Log.e(TAG, "RSA key pair error");
        }

        // Encode the original data with RSA private key
        byte[] encodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA");
            c.init(Cipher.ENCRYPT_MODE, privateKey);
            encodedBytes = c.doFinal(input.getBytes());
        } catch (Exception e) {
            Log.e(TAG, "RSA encryption error");
        }
//        encodedstring = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
        // Decode the encoded data with RSA public key
        byte[] decodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA");
            c.init(Cipher.DECRYPT_MODE, publicKey);
            decodedBytes = c.doFinal(encodedBytes);
        } catch (Exception e) {
            Log.e(TAG, "RSA decryption error");
        }

        if (enc_dec.equals("ENC")){
            encodedstring = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
        }else {
            encodedstring= new String(decodedBytes);
        }
        return encodedstring;
    }

}
