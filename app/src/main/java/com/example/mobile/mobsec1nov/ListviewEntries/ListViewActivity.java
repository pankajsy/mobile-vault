package com.example.mobile.mobsec1nov.ListviewEntries;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mobile.mobsec1nov.R;
import com.example.mobile.mobsec1nov.androidsqlite.DatabaseHandler;
import com.example.mobile.mobsec1nov.androidsqlite.Entryfield;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    ListView listView ;
    SwipeRefreshLayout swipeLayout;
    ListviewAdapter adapter;
    DatabaseHandler db;
    ArrayList<Entryfield> entry = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listviewlayout);
        listView = (ListView) findViewById(R.id.list);
        swipeLayout = (SwipeRefreshLayout)findViewById(R.id.srl_container);
        db = new DatabaseHandler(ListViewActivity.this);
        if (db.getCount()==0){
            db.addEntry(
                    new Entryfield(
                            "Folder name Default entry",
                            "example@domain.com",
                            "Username",
                            "Password",
                            "Url/Link of the website",
                            "Title of this entry",
                            "Notes May include important/confidential information. "+
                            "All the files related to this entry will sit here in your local storage by folder name"
                    ));
        }

            entry = db.getAllEntry();
            adapter = new ListviewAdapter(ListViewActivity.this, entry);
            listView.setAdapter(adapter);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Add a New Entry", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    Intent intent = new Intent(ListViewActivity.this, EnterDetails.class);
                    intent.putExtra("Entryfield","Add");
                    startActivity(intent);
                }
            });

        swipeLayout.setOnRefreshListener(this);

    }
    @Override
    public void onRefresh() {
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    entry = db.getAllEntry();
                    adapter = new ListviewAdapter(ListViewActivity.this, entry);
                    listView.setAdapter(adapter);
                    swipeLayout.setRefreshing(false);
                }
            }, 3000);
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong",Toast.LENGTH_SHORT).show();
        }
    }

}
