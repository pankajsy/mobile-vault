package com.example.mobile.mobsec1nov.ListviewEntries;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mobile.mobsec1nov.R;
import com.example.mobile.mobsec1nov.androidsqlite.DatabaseHandler;
import com.example.mobile.mobsec1nov.androidsqlite.Entryfield;

import java.util.ArrayList;

public class ListviewAdapter extends ArrayAdapter<Entryfield> {
//	 protected ImageLoader imageLoader ;
     Context context;
     LayoutInflater inflater;
	 ArrayList<Entryfield> entries;
     Entryfield entry = new Entryfield();
     public ListviewAdapter(Context context, ArrayList<Entryfield> _entries){
	    	super(context,0,_entries);
	    	this.entries = _entries;
	    	this.context = context;
//	    	this.imageLoader = ImageLoader.getInstance();
     }
 
	 @Override
     public void notifyDataSetChanged() {
	        super.notifyDataSetChanged();
	 }
	
	 static class ViewHolderItem 
	    {
	        TextView title, folder, Id;
			LinearLayout field;
			ImageView delete;
	    }
	 
	 @Override
     public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolderItem viewHolder;
		 entry = entries.get(position);
        if(convertView == null){
	       	inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.listviewadapter, parent, false);
	       	viewHolder = new ViewHolderItem();
	       	viewHolder.title = (TextView) convertView.findViewById(R.id.title);
	       	viewHolder.Id = (TextView) convertView.findViewById(R.id.Id);
	       	viewHolder.field = (LinearLayout) convertView.findViewById(R.id.field);
	       	viewHolder.folder = (TextView) convertView.findViewById(R.id.folder);
	       	viewHolder.delete = (ImageView) convertView.findViewById(R.id.delete);
	        convertView.setTag(viewHolder);

        }
        else{
        	viewHolder = (ViewHolderItem) convertView.getTag();
        }
        
		if (entry.getTitle()!=null){
			viewHolder.folder.setText(entry.getFolderName());
			viewHolder.Id.setText(String.valueOf(entry.getID()));
			viewHolder.title.setText(entry.getTitle());
			viewHolder.field.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Entryfield onerntry = entries.get(position);
					Intent intent = new Intent(context, EnterDetails.class);
					intent.putExtra("Value",onerntry);
					intent.putExtra("Entryfield", "Update");
					context.startActivity(intent);
				}
			});
			viewHolder.delete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Entryfield onerntry = entries.get(position);
					DatabaseHandler db = new DatabaseHandler(context);
					db.deleteEntry(onerntry);
					entries.remove(position);
					notifyDataSetChanged();
					Toast.makeText(context, "Deleted entry!", Toast.LENGTH_LONG).show();
				}
			});

		}


        return convertView;
    }
    
   
}
