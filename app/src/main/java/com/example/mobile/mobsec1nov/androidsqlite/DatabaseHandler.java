package com.example.mobile.mobsec1nov.androidsqlite;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.example.mobile.mobsec1nov.cryptoalgorithm.AESClass_Salt2;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "entryManager";
	// Contacts table name
	private static final String TABLE_ENTRY = "Entryfield";
	private static final String KEY_ID = "id";
	private static final String KEY_FOLDER = "folder";
	private static final String KEY_EMAIL = "email";
	private static final String KEY_USER = "username";
	private static final String KEY_PASS = "password";
	private static final String KEY_URL = "url";
	private static final String KEY_TITLE = "title";
	private static final String KEY_NOTES = "notes";
	private static Context CONTEXT;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		CONTEXT = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_ENTRY_TABLE = "CREATE TABLE " + TABLE_ENTRY + "("
				+ KEY_ID + " INTEGER PRIMARY KEY,"
				+ KEY_FOLDER + " TEXT,"
				+ KEY_EMAIL + " TEXT,"
				+ KEY_USER + " TEXT,"
				+ KEY_PASS+ " TEXT,"
				+ KEY_URL + " TEXT,"
				+ KEY_TITLE + " TEXT,"
				+ KEY_NOTES + " TEXT"+ ")";
		db.execSQL(CREATE_ENTRY_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRY);
		onCreate(db);
	}

	public void addEntry(Entryfield entry) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FOLDER, AESClass_Salt2.encryptString(CONTEXT, entry.getFolderName()));
		values.put(KEY_EMAIL, AESClass_Salt2.encryptString(CONTEXT,entry.getEmail()));
		values.put(KEY_USER, AESClass_Salt2.encryptString(CONTEXT,entry.getUsername()));
		values.put(KEY_PASS, AESClass_Salt2.encryptString(CONTEXT,entry.getPassword()));
		values.put(KEY_URL, AESClass_Salt2.encryptString(CONTEXT,entry.getUrl()));
		values.put(KEY_TITLE, AESClass_Salt2.encryptString(CONTEXT,entry.getTitle()));
		values.put(KEY_NOTES, AESClass_Salt2.encryptString(CONTEXT,entry.getNotes()));


		// Inserting Row
		db.insert(TABLE_ENTRY, null, values);
		db.close(); // Closing database connection
	}

	public Entryfield getEntry(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(TABLE_ENTRY, new String[] {
						KEY_ID,
						KEY_FOLDER,
						KEY_EMAIL,
						KEY_USER,
						KEY_PASS,
						KEY_URL,
						KEY_TITLE,
						KEY_NOTES }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();
			Entryfield entry = new Entryfield(
				Integer.parseInt(cursor.getString(0)),
				cursor.getString(1),
				cursor.getString(2),
				cursor.getString(3),
				cursor.getString(4),
				cursor.getString(5),
				cursor.getString(6),
				cursor.getString(7));
		return entry;
	}
	
	// Getting All Entryfield
	public ArrayList<Entryfield> getAllEntry() {
		ArrayList<Entryfield> entryList = new ArrayList<Entryfield>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ENTRY;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				Entryfield entry = new Entryfield();
				entry.setID(Integer.parseInt(cursor.getString(0)));
				entry.setFolderName(AESClass_Salt2.decryptString(CONTEXT,cursor.getString(1)));
				entry.setEmail(AESClass_Salt2.decryptString(CONTEXT,cursor.getString(2)));
				entry.setUsername(AESClass_Salt2.decryptString(CONTEXT,cursor.getString(3)));
				entry.setPassword(AESClass_Salt2.decryptString(CONTEXT,cursor.getString(4)));
				entry.setUrl(AESClass_Salt2.decryptString(CONTEXT,cursor.getString(5)));
				entry.setTitle(AESClass_Salt2.decryptString(CONTEXT,cursor.getString(6)));
				entry.setNotes(AESClass_Salt2.decryptString(CONTEXT,cursor.getString(7)));
				// Adding entry to list
				entryList.add(entry);
			} while (cursor.moveToNext());
		}

		// return enrty list
		return entryList;
	}

	// Updating single entry
	public int updateEntry(Entryfield entry) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FOLDER, AESClass_Salt2.encryptString(CONTEXT,entry.getFolderName()));
		values.put(KEY_EMAIL, AESClass_Salt2.encryptString(CONTEXT,entry.getEmail()));
		values.put(KEY_USER, AESClass_Salt2.encryptString(CONTEXT,entry.getUsername()));
		values.put(KEY_PASS, AESClass_Salt2.encryptString(CONTEXT,entry.getPassword()));
		values.put(KEY_URL, AESClass_Salt2.encryptString(CONTEXT,entry.getUrl()));
		values.put(KEY_TITLE, AESClass_Salt2.encryptString(CONTEXT,entry.getTitle()));
		values.put(KEY_NOTES, AESClass_Salt2.encryptString(CONTEXT,entry.getNotes()));

		// updating row
		return db.update(TABLE_ENTRY, values, KEY_ID + " = ?",
				new String[] { String.valueOf(entry.getID()) });
	}

	// Deleting single entry
	public void deleteEntry(Entryfield entry) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_ENTRY, KEY_ID + " = ?",
				new String[] { String.valueOf(entry.getID()) });
		db.close();
	}


	// Getting contacts Count
	public long getCount() {
		String countQuery = "SELECT  * FROM " + TABLE_ENTRY;
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT COUNT(*) FROM " + TABLE_ENTRY;
		SQLiteStatement statement = db.compileStatement(sql);
		long count = statement.simpleQueryForLong();
		return count;
	}

}
