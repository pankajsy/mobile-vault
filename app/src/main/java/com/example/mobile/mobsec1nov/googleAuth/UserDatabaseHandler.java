//package com.example.mobile.mobsec1nov.googleAuth;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;
//
//import java.util.ArrayList;
//
//public class UserDatabaseHandler extends SQLiteOpenHelper {
//
//    private static final int DATABASE_VERSION = 1;
//    private static final String TAG_STRING = UserDatabaseHandler.class.getName();
//    private static final String DATABASE_NAME = "UsersManager";
//    private static final String TABLE_USERS = "Users";
//    private static final String KEY_ID = "id";
//    private static final String KEY_UNAME = "name";
//    private static final String KEY_FNAME = "firstname";
//    private static final String KEY_LNAME = "email";
//    private static final String KEY_TOKEN = "token";
//    private final ArrayList<User> User_list = new ArrayList<User>();
//    private User Userlogged = new User();
//
//    public UserDatabaseHandler(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//    }
//
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
//                + KEY_ID + " INTEGER," + KEY_UNAME + " TEXT,"
//                + KEY_FNAME + " TEXT," + KEY_LNAME + " TEXT," + KEY_TOKEN + " TEXT" + ");";
//        db.execSQL(CREATE_USERS_TABLE);
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        Log.w(UserDatabaseHandler.class.getName(),
//                "Upgrading database from version " + oldVersion + " to "
//                        + newVersion + ", which will destroy all old data");
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
//
//        // Create tables again
//        onCreate(db);
//    }
//
//    // Add a single User
//    public void Add_User(User user) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_ID, user.getID());
//        values.put(KEY_UNAME, user.getUName());
//        values.put(KEY_FNAME, user.getFname());
//        values.put(KEY_LNAME, user.getLname());
//        values.put(KEY_TOKEN, user.getToken());
//        db.insert(TABLE_USERS, null, values);
//        db.close();
//    }
//
//    // Getting single User from username
//    public User Get_User_single(String username) {
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.query(TABLE_USERS, new String[]{KEY_ID,
//                        KEY_UNAME, KEY_FNAME, KEY_LNAME, KEY_TOKEN}, KEY_UNAME + "=?",
//                new String[]{String.valueOf(username)}, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        User User = new User(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
//        // return User
//        cursor.close();
//        db.close();
//
//        return User;
//    }
//
//    public User Get_User() {
//        try {
//            String selectQuery = "SELECT  * FROM " + TABLE_USERS;
//            SQLiteDatabase db = this.getWritableDatabase();
//            Cursor cursor = db.rawQuery(selectQuery, null);
//            if (cursor.moveToFirst()) {
//                User user = new User();
//                user.setID(Integer.parseInt(cursor.getString(0)));
//                user.setUName(cursor.getString(1));
//                user.setFname(cursor.getString(2));
//                user.setLname(cursor.getString(3));
//                user.setToken(cursor.getString(4));
//                Userlogged = user;
//            }
//
//            cursor.close();
//            db.close();
//            return Userlogged;
//        } catch (Exception e) {
//            Log.e(TAG_STRING, "" + e);
//        }
//
//        return Userlogged;
//    }
//
//    public ArrayList<User> Get_Users() {
//        try {
//            User_list.clear();
//            String selectQuery = "SELECT  * FROM " + TABLE_USERS;
//            SQLiteDatabase db = this.getWritableDatabase();
//            Cursor cursor = db.rawQuery(selectQuery, null);
//            if (cursor.moveToFirst()) {
//                do {
//                    User User = new User();
//                    User.setID(Integer.parseInt(cursor.getString(0)));
//                    User.setUName(cursor.getString(1));
//                    User.setFname(cursor.getString(2));
//                    User.setLname(cursor.getString(3));
//                    User_list.add(User);
//                } while (cursor.moveToNext());
//            }
//
//            cursor.close();
//            db.close();
//            return User_list;
//        } catch (Exception e) {
//            Log.e(TAG_STRING, "" + e);
//        }
//
//        return User_list;
//    }
//
//    public int Update_User(User User) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(KEY_UNAME, User.getUName());
//        values.put(KEY_FNAME, User.getFname());
//        values.put(KEY_LNAME, User.getLname());
//        values.put(KEY_TOKEN, User.getToken());
//        return db.update(TABLE_USERS, values, KEY_ID + " = ?",
//                new String[]{String.valueOf(User.getID())});
//    }
//
//    public void Delete_User(int id) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_USERS, KEY_ID + " = ?",
//                new String[]{String.valueOf(id)});
//        db.close();
//    }
//
//
//}
